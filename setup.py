from setuptools import setup

setup(
    name='homing_pigeon_client',
    version='0.1.0',
    packages=['homing_pigeon_client'],
    # url='https://github.com/ConvergysLabs/arctic-tern',
    # license='MIT',
    author='Eric Grunzke',
    author_email='eric.grunzke@concentrix.com',
    description='Python client for Homing Pigeon',
    install_requires=[
        'lomond',
        'gevent'
    ]

)
