import json
import os
import pprint
from typing import List

import gevent
from gevent.event import AsyncResult
from lomond import WebSocket
from lomond.events import Poll, Pong, Ping
from lomond.persist import persist

from .log import log

is_debug = os.getenv('ENVIRONMENT') != 'production'
homing_pigeon_host = os.getenv('HOMING_PIGEON_HOST', 'localhost:8080')
HEARTBEAT_SECONDS = 30.0

_web_socket = WebSocket(f'ws://{homing_pigeon_host}/socket')


def _web_socket_listener(web_socket: WebSocket):
    for event_raw in persist(web_socket, poll=HEARTBEAT_SECONDS):
        boring = isinstance(event_raw, Ping) or isinstance(event_raw, Pong) or isinstance(event_raw, Poll)
        if not boring:
            log.debug(f'Socket Event: {event_raw}')

        if event_raw.name == 'text':
            event = json.loads(event_raw.text)
            _notify_clients(event)


def _notify_clients(event):
    tenant = event['tenant']
    channel = event['channel']
    type_ = event['type']
    body = event['body']
    for client in clients:
        if client.tenant == tenant and client.channel == channel and client.type_ == type_:
            client.results.set(body)


class HomingPigeonClient:
    def __init__(self, auth_header, tenant, channel, type_):
        self.auth_header = auth_header
        self.tenant = tenant
        self.channel = channel
        self.type_ = type_
        self.results = AsyncResult()

    def __eq__(self, other):
        if other.__class__ is self.__class__:
            s = (self.auth_header, self.tenant, self.channel, self.type_)
            o = (other.auth_header, other.tenant, other.channel, other.type_)
            return s == o
        return NotImplemented

    def _send(self, message=None, action=None):
        kwargs = {}
        if action:
            kwargs['action'] = action
        if message:
            kwargs['message'] = message
            kwargs['type'] = self.type_

        if kwargs:
            payload = {'tenant': self.tenant, 'channel': self.channel, 'authorization': self.auth_header, **kwargs}
            pretty = pprint.pformat(payload)
            log.debug(f'Sending message:\n{pretty}')
            _web_socket.send_json(payload)

    def send(self, message):
        self._send(message=message, action='send')

    def join(self):
        self._send(action='join')

    def leave(self):
        self._send(action='leave')
        clients.remove(self)

    def wait(self):
        """Sleeps execution of this greenlet until a result is received from homing pigeon.

        Only call this in a greenlet! This prevents further execution, which is fine in a
        coroutine but very bad in the main thread.
        """
        return self.asyn().get()

    def asyn(self):
        clients.append(self)
        return self.results


clients: List[HomingPigeonClient] = []
log.info('connecting to homing-pigeon')
gevent.spawn(_web_socket_listener, _web_socket)
