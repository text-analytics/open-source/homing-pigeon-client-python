import logging

log = logging.getLogger('homing-pigeon-client')

# If there are no handlers, add one to write to console.
if not log.hasHandlers():
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('[%(asctime)s] [%(levelname)s] [homing-pigeon-client] %(message)s', datefmt="%m-%d %H:%M:%S")
    ch.setFormatter(formatter)
    log.setLevel(logging.DEBUG)
    log.addHandler(ch)
